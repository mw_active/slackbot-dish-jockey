# Slackbot Dish Jockey

Chorebot is adapted from [Drake Cooper labs' project](https://bitbucket.org/drakecooper/chorebot/src/master/). It uses Google sheets and the Slack API to build a bot to randomly select and assign Slack users to predefined chores.

## Features

+ For each chore, chorebot will randomly select a user from the "people" sheet.
+ Chorebot will mention the user in a message, posted in the `#kitchen-karma` channel.
+ Previously assigned users will be kept track in the "people_already_assigned" sheet, to maximize fairness of chore distribution.

_Note: Chores are assigned in the afternoon for the following day. Users can use Slack reminders if they want an additional notification in the morning of their duties._

## Getting Started

### Google Sheet - People and Chore List

You can upload the `People and Chore List -Template.xlsx` to Google Sheets as a start. Follow the instructions below to fill out the required info.

1. Create a new spreadsheet in Google Sheets. It needs 3 sheets, "people", "chores", and "people_already_assigned" (the last one will be empty).
2. In the "people" sheet, populate the first column with Slack usernames, and the second column with user IDs.*
3. In the "chores" sheet, populate the first column with the chore you want to assign, and the second column with the desired frequency (ie. "daily", "weekly", or "monthly").

_*Note: Mentions with usernames via API calls no longer work as of September 11, 2017 ([source](https://api.slack.com/changelog/2017-09-the-one-about-usernames)). Instead, user IDs must be used._

### Slack Custom App Integration

1. In Slack, under your organization name, choose "Configure Apps > Custom Integrations > Incoming WebHooks". Then click "Add Configuration".
2. Leave this webpage open. You will need the Webhook URL in the next step!
3. Scroll down to customize the name and icon of your slackbot.

### Setting Up the Script

1. Open the Script Editor (Tools > Script editor...) and copy/paste the code in `chorebot.js`.
2. Copy the Webhook URL to the `settings.slackWebhookURL` variable in the script.
2. Test it out by selecting the `delegateChores()` function in the script editor, then clicking "run" (play button).
3. Add some triggers in the script editor to run the script automatically.
4. Build on chorebot by adding your own custom functions.

## Development Notes

+ Only give your Google Sheet access as-needed, as you will be storing a somewhat private Webhook URL there. If, somehow your Webhook URL gets compromised, you can always regenerate it in Slack.

### Setting up Clasp for Local

To edit the Google Scripts files in your local text editor instead of the Apps Script platform, you can use [clasp](https://developers.google.com/apps-script/guides/clasp).

```bash
# Clone the project to local folder
clasp clone <scriptId>

# Pull changes from project to local
clasp pull

# Push changes from local to project
clasp push
```

### Settings.js

This file contains your secret Slack webhook. If committing to a public repository, it's convenient to tell git to ignore this file so it doesn't get accidentally committed/pushed.

```bash
# Ignore changes to this file
git update-index --skip-worktree Settings.js

# Re-enable tracking changes to this file
git update-index --no-skip-worktree Settings.js
```

## Resources

+ Slack API documentation
    + [Formatting messages](https://api.slack.com/docs/message-formatting#linking_to_channels_and_users)
    + [Message buttons](https://api.slack.com/docs/message-buttons)

## Contributors

+ Justin Lam