function delegateChores() {

  // Get the spreadsheet
  var ss = SpreadsheetApp.getActiveSpreadsheet();

  // Get the list of people
  var s_people = ss.getSheetByName('people');
  var people = s_people.getDataRange().getValues();

  // Get the list of chores
  var s_chores = ss.getSheetByName('chores');
  var chores = s_chores.getDataRange().getValues();

  // Get list of previously assigned people
  var s_people_assigned = ss.getSheetByName('people_already_assigned');
  var people_assigned = s_people_assigned.getDataRange().getValues();

  // Set up an array to store already assigned people
  var assigned = [];

  // Get the current date
  var today = new Date();
  var dayOfWeek = today.getDay();
  var dayOfMonth = today.getDate();

  var chore;

  // Return a random person index that hasn't been chosen yet
  function uniqueRandPerson(arr) {
    var e;

    // Only run this loop if there are still people left to choose from
    if(arr.length < people.length) {
      do {
        e = Math.floor(Math.random() * people.length);
        Logger.log("Random number: " + e);
      } while (arr.indexOf(e) !== -1);
    } else {

      // Return -1 if everyone has already been chosen
      e = -1;
    }

    return e;
  }

  // Return true if a given person is in the given list, false if not.
  function isPersonInList(target_person, people_list) {
    var found = false;

    for (var i=0; i < people_list.length; i++) {
      if (target_person == people_list[i][0]) {
        found = true;
      }
    }

    return found;
  }

  // Exit if it's a weekend
  if(dayOfWeek == 6 || dayOfWeek == 0) {
    Logger.log("Today is a weekend. Exiting.");
    return;
  }

  // Loop through the chores, everything needs to get done!
  for(var i = 0; i < chores.length; i++) {

    // Reset sheet
    if (people_assigned.length >= people.length) {
      Logger.log("Clearing sheet and names");
      s_people_assigned.clear();
      people_assigned = s_people_assigned.getDataRange().getValues();
    }

    // Grab a random person index. Keep trying until someone new is picked
    do {
      var rand_person = uniqueRandPerson(assigned);
      Logger.log("Proposed: " + people[rand_person][0]);
    }
    while (isPersonInList(people[rand_person][0], people_assigned) == true);

    Logger.log("Selected: " + people[rand_person][0]);

    // If the chore is blank, move along
    if(chores[i][0] === '') {
      continue;
    }

    if(rand_person !== -1) {

      // Assign the lucky winner to the current chore, if it matches the correct frequency
      if(chores[i][1] === 'daily' || ( chores[i][1] === 'weekly' && dayOfWeek === 1 ) || ( chores[i][1] === 'monthly' && dayOfMonth < 7 && dayOfWeek === 1) ) {

        Logger.log("Assigning task");

        // If it's Friday, append additional note to task
        if (dayOfWeek == 5) {
          chore = chores[i][0] + ' (ie. on Monday)';
        }
        else {
          chore = chores[i][0];
        }

        assign(people[rand_person][1], chore);

        // Add the assignee to the assigned array so they don't get chosen again
        assigned.push(rand_person);

        // Also add assignee to third spreadsheet to keep track of people already assigned on a previous day
        ss.getSheetByName('people_already_assigned').appendRow([people[rand_person][0]])
      }
      else {
        Logger.log("ERROR: No task assigned since no matching frequency for: " + chores[i][1]);
      }

    } else {

      Logger.log('There are more chores than people!');

    }
  }

}

var payload = {
  'link_names': 1
}

var opts = {
  'method': 'post',
  'contentType': 'application/json'
}

// Send a message to the #chores slack channel, assigning a @user to a chore.
function assign(user_id, chore) {

  // Need to use user_id as of Sept 2017 (source: https://api.slack.com/changelog/2017-09-the-one-about-usernames)
  var msg = '<@' + user_id + '>, ' + chore;
  Logger.log(msg);

  talk(msg);

}

// Utility fn to send an ad hoc message as Rosie
function talk(msg) {

  payload.text = msg;
  opts.payload = JSON.stringify(payload);

  result = UrlFetchApp.fetch(settings.slackWebhookURL, opts);
  Logger.log("Response: " + result);

}
